﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exo3
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, int> bar = new Dictionary<string, int>();
            bool encore = true;
            while (encore)
            {
                Console.WriteLine("Ajouter une bière ou stop pour arrêter :");
                string biere = Console.ReadLine();
                if (biere == "stop")
                {
                    encore = false;
                }
                else
                {
                    Console.WriteLine($"Quelle quantité de {biere} ? ");
                    string nb = Console.ReadLine();
                    int quantite = int.Parse(nb);
                    bar.Add(biere, quantite);
                    Console.WriteLine("etat du stock:");
                    Console.Clear();
                    foreach(KeyValuePair<string, int> kvp in bar)
                    {
                        Console.WriteLine($"{kvp.Key}:{kvp.Value}");
                    }
                }


            }
        }
    }
}
