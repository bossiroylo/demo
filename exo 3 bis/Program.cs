﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exo_3_bis
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, int> bar = new Dictionary<string, int>();
            while (true)
            {
                #region Choix du menu
                Console.WriteLine("1.Ajouter");
                Console.WriteLine("2.Modifier");
                Console.WriteLine("3.Supprimer");
                int choix = int.Parse(Console.ReadLine());
                Console.Clear();
                #endregion

                #region choix 1
                if (choix == 1)
                {
                    Console.WriteLine("Quel produit voulez-vous ajouter ? ");
                    string nom = Console.ReadLine();
                    nom = nom.ToUpper();
                    while (bar.ContainsKey(nom))
                    {
                        Console.WriteLine("Le produit existe déjà, veuillez rajouter un autre produit");
                        nom = Console.ReadLine();
                    }
                    Console.WriteLine("Quelle quantité voulez-vous rajouter ? ");
                    string valeur = Console.ReadLine();
                    int nombre = int.Parse(valeur);
                    int nb;
                    while (!int.TryParse(valeur, out nb))
                    {
                        Console.WriteLine("Quantité non valide, veuillez mettre une quantité valide");
                        valeur = Console.ReadLine();
                    }
                    bar.Add(nom, nb); 
                    #endregion
                }
                #region choix 2
                else if (choix == 2)
                {
                    Console.WriteLine("Voici la liste de nos produit déjà encoder");
                    foreach (KeyValuePair<string, int> kvp in bar)
                    {
                        Console.WriteLine($"{kvp.Key}:{kvp.Value}");
                    }
                    Console.WriteLine("Quel produit voulez-vous modifier :");
                    string nom = Console.ReadLine();

                    Console.WriteLine("Quel stock");
                    int nombre = int.Parse(Console.ReadLine());
                    bar[nom.ToUpper()] = nombre;


                    Console.ReadKey();

                } 
                #endregion
                #region choix 3
                else if (choix == 3)
                {
                    Console.WriteLine("Voici la liste de nos produit déjà encoder");
                    foreach (KeyValuePair<string, int> kvp in bar)
                    {
                        Console.WriteLine($"{kvp.Key}:{kvp.Value}");
                    }
                    Console.WriteLine("Quel prodfuit voulez vous supprimer ?");
                    string nom = Console.ReadLine();
                    nom = nom.ToUpper();
                    bar.Remove(nom);
                } 
                #endregion
                Console.Clear();
                foreach (KeyValuePair<string, int>kvp in bar)
                {
                    Console.WriteLine($"{kvp.Key}:{kvp.Value}");
                }

            }
        }
    }
}
