﻿using System;

namespace exo0
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Entrez le nombre de seconde à convertir : ");
            string seconde = Console.ReadLine();
            int nbseconde = int.Parse(seconde);

            int heures = nbseconde / 3600;
            nbseconde = nbseconde % 3600;

            int minutes = nbseconde / 60;
            nbseconde = nbseconde % 60;

            int secondes = nbseconde;

            Console.WriteLine($"" +
                ${heures} heures, {minutes} minutes, {seconde} seconde.");
            Console.ReadKey();

        }
    }
}
